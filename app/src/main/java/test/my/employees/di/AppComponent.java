package test.my.employees.di;

import javax.inject.Singleton;

import dagger.Component;
import test.my.employees.ui.employees.EmployeesFragment;

/**
 * Created by Sergey Karleev on 04.11.2018.
 */
@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, RoomModule.class})
public interface AppComponent {

    void inject(EmployeesFragment employeesFragment);
}