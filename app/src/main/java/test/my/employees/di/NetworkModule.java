package test.my.employees.di;

import android.arch.lifecycle.ViewModelProvider;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import test.my.employees.common.EmployeesViewModelFactory;
import test.my.employees.data.locale.dao.EmployeesDao;
import test.my.employees.data.remote.api.EmployeesApi;
import test.my.employees.data.repositories.EmployeesRepository;

/**
 * Created by Sergey Karleev on 04.11.2018.
 */
@Module
public class NetworkModule {
    private static final String BASE_URL = "http://gitlab.65apps.com/65gb/static/raw/";

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
//         .connectTimeout(100, TimeUnit.SECONDS)
//                .writeTimeout(100, TimeUnit.SECONDS)
//                .readTimeout(300, TimeUnit.SECONDS);
        return client.build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson, OkHttpClient httpClient) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient)
                .build();
    }

    @Provides
    @Singleton
    EmployeesApi provideEmployeeService(Retrofit retrofit) {
        return retrofit.create(EmployeesApi.class);
    }

    @Provides
    @Singleton
    EmployeesRepository getEmployeesRepository(EmployeesApi employeesApi, EmployeesDao employeesDao) {
        return new EmployeesRepository(employeesApi, employeesDao);
    }

    @Provides
    @Singleton
    ViewModelProvider.Factory getViewModelFactory(EmployeesRepository repository) {
        return new EmployeesViewModelFactory(repository);
    }

}
