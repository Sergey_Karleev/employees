package test.my.employees.di;

import android.arch.persistence.room.Room;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import test.my.employees.data.locale.EmployeesDB;
import test.my.employees.data.locale.dao.EmployeesDao;

/**
 * Created by Sergey Karleev on 05.11.2018.
 */
@Module
public class RoomModule {

    private EmployeesDB employeesDB;

    public RoomModule(Context mContext) {
        employeesDB = Room.databaseBuilder(mContext, EmployeesDB.class, "db").build();
    }

    @Singleton
    @Provides
    EmployeesDB providesRoomDatabase() {
        return employeesDB;
    }

    @Singleton
    @Provides
    EmployeesDao providesEmployeesDao(EmployeesDB employeesDB) {
        return employeesDB.employeesDao();
    }
}
