package test.my.employees.common;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import test.my.employees.data.repositories.EmployeesRepository;
import test.my.employees.ui.employees.EmployeesViewModel;

/**
 * Created by Sergey Karleev on 04.11.2018.
 */
public class EmployeesViewModelFactory implements ViewModelProvider.Factory {
    private EmployeesRepository repository;

    @Inject
    public EmployeesViewModelFactory(EmployeesRepository repository) {
        this.repository = repository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(EmployeesViewModel.class))
            return (T) new EmployeesViewModel(repository);
        throw new IllegalArgumentException("Unknown class name");
    }
}
