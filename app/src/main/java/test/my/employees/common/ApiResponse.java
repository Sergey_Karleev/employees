package test.my.employees.common;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by Sergey Karleev on 04.11.2018.
 */
public class ApiResponse<T> {
    public final Status status;

    @Nullable
    public final T data;

    @Nullable
    public final String error;

    private ApiResponse(Status status, @Nullable T data, @Nullable String error) {
        this.status = status;
        this.data = data;
        this.error = error;
    }

    public static ApiResponse loading() {
        return new ApiResponse(Status.LOADING, null, null);
    }

    public static <T> ApiResponse success(@NonNull T data) {
        return new ApiResponse(Status.SUCCESS, data, null);
    }

    public static <T> ApiResponse error(String msg, @Nullable T data) {
        return new ApiResponse(Status.ERROR, data, msg);
    }

    public enum Status {
        LOADING,
        SUCCESS,
        ERROR,
        COMPLETED
    }
}
