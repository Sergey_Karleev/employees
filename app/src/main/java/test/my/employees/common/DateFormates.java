package test.my.employees.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created by Sergey Karleev on 05.11.2018.
 */
public enum DateFormates {
    ONE(Pattern.compile("^\\d{4}-\\d{2}-\\d{2}$"), "yyyy-MM-dd"),
    TWO(Pattern.compile("^\\d{2}-\\d{2}-\\d{4}$"), "dd-MM-yyyy"),
    THREE(Pattern.compile("^\\d{2}\\.\\d{2}\\.\\d{4}$"), "dd.MM.yyyy");

    private final String format;
    private final Pattern pattern;

    DateFormates(Pattern pattern, String format) {
        this.pattern = pattern;
        this.format = format;
    }

    public static SimpleDateFormat createSimpleDateFormat(String date) {
        if (date == null) return null;

        for (DateFormates df : values()) {
            if (df.pattern().matcher(date).matches())
                return new SimpleDateFormat(df.format);
        }
        return null;
    }

    public static String convertFormat(DateFormates to, String date) {
        if (to == null || date == null) return null;

        SimpleDateFormat formatFrom = createSimpleDateFormat(date);
        SimpleDateFormat formatTo = new SimpleDateFormat(to.format());

        if (formatFrom == null) return null;
        try {
            Date mDate = formatFrom.parse(date);
            return formatTo.format(mDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String format() {
        return format;
    }

    public Pattern pattern() {
        return pattern;
    }
}
