package test.my.employees.ui.detail;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.annimon.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.my.employees.R;

public class DetailEmployeeFragment extends Fragment {

//    @BindView(R.id.label_first_name)
//    TextView labelFirstName;
//    @BindView(R.id.label_last_name)
//    TextView labelLastName;
//    @BindView(R.id.label_birthday)
//    TextView labelBirthday;
//    @BindView(R.id.label_age)
//    TextView labelAge;
//    @BindView(R.id.label_specialty)
//    TextView labelSpecialty;

    @BindView(R.id.first_name)
    TextView firstName;
    @BindView(R.id.last_name)
    TextView lastName;
    @BindView(R.id.birthday)
    TextView birthday;
    @BindView(R.id.age)
    TextView age;
    @BindView(R.id.specialty)
    TextView specialty;

    public static DetailEmployeeFragment newInstance() {
        return new DetailEmployeeFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_employee, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        DetailsEmployeeViewModel mViewModel = ViewModelProviders.of(getActivity()).get(DetailsEmployeeViewModel.class);
        mViewModel.getEmployeeData()
                .observe(this, employee -> {
                    if (employee == null) return;
                    firstName.setText(employee.getFirstName());
                    lastName.setText(employee.getLastName());
                    birthday.setText(employee.getFormattedBirthday());
                    age.setText(employee.getAge());
                    String specialityText = StringUtils.join(Stream
                            .of(employee.getSpecialtyList())
                            .flatMap(sp -> Stream.of(sp.getName()))
                            .toList(), ", ");

                    specialty.setText(specialityText);
                });
    }

}
