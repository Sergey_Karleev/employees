package test.my.employees.ui.employees;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import test.my.employees.common.ApiResponse;
import test.my.employees.data.locale.models.Employee;
import test.my.employees.data.repositories.EmployeesRepository;

public class EmployeesViewModel extends ViewModel {

    private EmployeesRepository repository;
    private LiveData<ApiResponse<List<Employee>>> employeesList;


    public EmployeesViewModel(EmployeesRepository repository) {
        this.repository = repository;
        if (employeesList == null)
            employeesList = repository.getEmployeesList();
    }

    LiveData<ApiResponse<List<Employee>>> getEmployeesList() {
        repository.refreshEmpoyeesList();
        return this.employeesList;
    }
}
