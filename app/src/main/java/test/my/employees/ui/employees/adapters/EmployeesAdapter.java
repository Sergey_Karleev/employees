package test.my.employees.ui.employees.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.my.employees.R;
import test.my.employees.data.locale.models.Employee;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

/**
 * Created by Sergey Karleev on 05.11.2018.
 */
public class EmployeesAdapter extends ExpandableRecyclerViewAdapter<
        EmployeesAdapter.SpecialtyViewHolder, EmployeesAdapter.EmployeeViewHolder> {

    private final OnEmployeeSelectedListener listener;

    public EmployeesAdapter(List<? extends ExpandableGroup> groups, OnEmployeeSelectedListener listener) {
        super(groups);
        this.listener = listener;
    }

    private static String normalize(String text) {
        return StringUtils.capitalize(StringUtils.lowerCase(text));
    }

    @Override
    public SpecialtyViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_simple_expandable_title, parent, false);

        return new SpecialtyViewHolder(view);
    }

    @Override
    public EmployeeViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_employee, parent, false);

        return new EmployeeViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(EmployeeViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        final Employee employee = (Employee) group.getItems().get(childIndex);
        holder.root.setOnClickListener(view -> {
            if (listener != null) listener.onSelect(holder, employee);
        });
        holder.firstName.setText(normalize(employee.getFirstName()));
        holder.lastName.setText(normalize(employee.getLastName()));
        holder.age.setText(employee.getAge());
    }

    @Override
    public void onBindGroupViewHolder(SpecialtyViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setText(group);
    }

    @FunctionalInterface
    public interface OnEmployeeSelectedListener {
        void onSelect(EmployeeViewHolder holder, Employee employee);
    }

    public static class SpecialityAdapter extends ExpandableGroup<Employee> {
        public SpecialityAdapter(String title, List<Employee> items) {
            super(title, items);
        }
    }

    public class SpecialtyViewHolder extends GroupViewHolder {

        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.arrow)
        ImageView arrow;

        public SpecialtyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setText(ExpandableGroup group) {
            text.setText(group.getTitle());
        }

        @Override
        public void expand() {
            RotateAnimation rotate =
                    new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
            rotate.setDuration(300);
            rotate.setFillAfter(true);
            arrow.setAnimation(rotate);
        }

        @Override
        public void collapse() {
            RotateAnimation rotate =
                    new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
            rotate.setDuration(300);
            rotate.setFillAfter(true);
            arrow.setAnimation(rotate);
        }
    }

    public class EmployeeViewHolder extends ChildViewHolder {

        @BindView(R.id.first_name)
        public TextView firstName;
        @BindView(R.id.last_name)
        public TextView lastName;
        @BindView(R.id.age)
        public TextView age;
        @BindView(R.id.root)
        ViewGroup root;

        public EmployeeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void onBind(Employee employee) {
            firstName.setText(employee.getFirstName());
            lastName.setText(employee.getLastName());
            age.setText(employee.getFormattedBirthday());
        }
    }
}
