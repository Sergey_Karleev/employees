package test.my.employees.ui.detail;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import test.my.employees.data.locale.models.Employee;

/**
 * Created by Sergey Karleev on 05.11.2018.
 */
public class DetailsEmployeeViewModel extends ViewModel {
    private MutableLiveData<Employee> employeeData = new MutableLiveData<>();

    public LiveData<Employee> getEmployeeData() {
        return employeeData;
    }

    public void setEmployee(Employee employee) {
        this.employeeData.postValue(employee);
    }
}
