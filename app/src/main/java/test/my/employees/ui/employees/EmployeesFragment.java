package test.my.employees.ui.employees;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.my.employees.EmployeesApp;
import test.my.employees.R;
import test.my.employees.common.ApiResponse;
import test.my.employees.common.EmployeesViewModelFactory;
import test.my.employees.data.locale.models.Employee;
import test.my.employees.data.locale.models.Specialty;
import test.my.employees.ui.MainActivity;
import test.my.employees.ui.detail.DetailEmployeeFragment;
import test.my.employees.ui.detail.DetailsEmployeeViewModel;
import test.my.employees.ui.employees.adapters.EmployeesAdapter;

import static android.content.ContentValues.TAG;

public class EmployeesFragment extends Fragment {

    @Inject
    EmployeesViewModelFactory employeesViewModelFactory;
    @BindView(R.id.recycler)
    RecyclerView recyclerView;
    @BindView(R.id.text_empty)
    TextView empty;
    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout refreshLayout;
    private EmployeesViewModel mViewModel;
    private DetailsEmployeeViewModel detailsViewModel;
    private EmployeesAdapter employeesAdapter;

    public static EmployeesFragment newInstance() {
        return new EmployeesFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_employees, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initViews();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (employeesAdapter != null) employeesAdapter.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (employeesAdapter != null) employeesAdapter.onRestoreInstanceState(savedInstanceState);
    }

    private void initViews() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        RecyclerView.ItemAnimator animator = recyclerView.getItemAnimator();
        if (animator instanceof DefaultItemAnimator) {
            ((DefaultItemAnimator) animator).setSupportsChangeAnimations(false);
        }

        refreshLayout.setOnRefreshListener(() -> {
            mViewModel.getEmployeesList();
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FragmentActivity activity = getActivity();

        ((EmployeesApp) activity.getApplication()).getAppComponent().inject(this);

        detailsViewModel = ViewModelProviders.of(activity).get(DetailsEmployeeViewModel.class);

        mViewModel = ViewModelProviders.of(activity, employeesViewModelFactory).get(EmployeesViewModel.class);
        mViewModel.getEmployeesList().observe(this, employees -> {
            if (ApiResponse.Status.SUCCESS.equals(employees.status)) {
                setDataToRecycler(employees.data);
            } else if (ApiResponse.Status.ERROR.equals(employees.status)) {
                setDataToRecycler(employees.data);
                Toast.makeText(activity, "Loading from cache...\n" + employees.error, Toast.LENGTH_LONG).show();
            }

            empty.setVisibility(employees.data != null && employees.data.size() > 1 ? View.GONE : View.VISIBLE);
            refreshLayout.setRefreshing(ApiResponse.Status.LOADING.equals(employees.status));
        });
    }

    private void setDataToRecycler(List<Employee> data) {
        Log.d(TAG, "setDataToRecycler: ");
        List<Specialty> specialties = Stream.of(data)
                .flatMap(employee -> Stream.of(employee.getSpecialtyList()))
                .distinct()
                .toList();


        ArrayList<EmployeesAdapter.SpecialityAdapter> specialityAdapters = new ArrayList<>();

        Stream.of(specialties)
                .forEach(specialty -> {
                    List<Employee> employees = Stream.of(data)
                            .filter(value -> value.getSpecialtyList().contains(specialty))
                            .toList();

                    specialityAdapters
                            .add(new EmployeesAdapter.SpecialityAdapter(specialty.getName(), employees));
                });

        employeesAdapter = new EmployeesAdapter(specialityAdapters, this::onEmployeeSelected);
        recyclerView.setAdapter(employeesAdapter);
        employeesAdapter.notifyDataSetChanged();
    }

    void onEmployeeSelected(EmployeesAdapter.EmployeeViewHolder holder, Employee employee) {
        detailsViewModel.setEmployee(employee);
        DetailEmployeeFragment detailEmployeeFragment = new DetailEmployeeFragment();
        ((MainActivity) getActivity()).changeFragment(detailEmployeeFragment);
    }
}
