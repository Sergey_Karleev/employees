package test.my.employees;

import android.app.Application;
import android.content.Context;

import test.my.employees.di.AppComponent;
import test.my.employees.di.AppModule;
import test.my.employees.di.DaggerAppComponent;
import test.my.employees.di.NetworkModule;
import test.my.employees.di.RoomModule;

/**
 * Created by Sergey Karleev on 04.11.2018.
 */
public class EmployeesApp extends Application {

    private AppComponent components;

    @Override
    public void onCreate() {
        super.onCreate();
        components = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .roomModule(new RoomModule(getApplicationContext()))
                .build();
    }

    public AppComponent getAppComponent() {
        return components;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
}
