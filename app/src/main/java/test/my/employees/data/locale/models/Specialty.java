package test.my.employees.data.locale.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergey Karleev on 04.11.2018.
 */
public class Specialty {
    @SerializedName("specialty_id")
    private int id;

    private String name;

    public Specialty() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        try {
            return this.id == ((Specialty) obj).id;
        } catch (Exception e) {
            return super.equals(obj);
        }
    }

    @Override
    public int hashCode() {
        return Integer.valueOf(id).hashCode();
    }
}
