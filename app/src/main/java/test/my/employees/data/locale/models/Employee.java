package test.my.employees.data.locale.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.TypeConverters;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import test.my.employees.common.DateFormates;
import test.my.employees.data.locale.GsonConverter;

/**
 * Created by Sergey Karleev on 04.11.2018.
 */
@Entity(primaryKeys = {"firstName", "lastName", "specialtyList"})
public class Employee implements Parcelable {
    public static final Creator<Employee> CREATOR = new Creator<Employee>() {
        @Override
        public Employee createFromParcel(Parcel in) {
            return new Employee(in);
        }

        @Override
        public Employee[] newArray(int size) {
            return new Employee[size];
        }
    };
    @SerializedName("f_name")
    @NonNull
    private String firstName = "";
    @SerializedName("l_name")
    @NonNull
    private String lastName = "";
    @Nullable
    private String birthday;

    @SerializedName("specialty")
    @TypeConverters(GsonConverter.class)
    @NonNull
    private List<Specialty> specialtyList;
    @SerializedName("avatr_url")
    @Nullable
    private String avatarUrl;

    public Employee() {
    }

    protected Employee(Parcel in) {
        firstName = in.readString();
        lastName = in.readString();
        birthday = in.readString();
        avatarUrl = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(birthday);
        dest.writeString(avatarUrl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getFirstName() {
        return normalize(firstName);
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return normalize(lastName);
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getFormattedBirthday() {
        String bDay = DateFormates.convertFormat(DateFormates.THREE, birthday);
        return bDay == null ? "-" : bDay + " г.";
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public List<Specialty> getSpecialtyList() {
        return specialtyList;
    }

    public void setSpecialtyList(List<Specialty> specialtyList) {
        this.specialtyList = specialtyList;
    }

    public String getAge() {
        if (StringUtils.isEmpty(birthday)) return "-";

        SimpleDateFormat format = DateFormates.createSimpleDateFormat(birthday);
        if (format == null) return "undefined format";

        try {
            Calendar currentCalendar = Calendar.getInstance();
            Calendar birthdayCalendar = Calendar.getInstance();
            birthdayCalendar.setTime(format.parse(birthday));
            int diff = currentCalendar.get(Calendar.YEAR) - birthdayCalendar.get(Calendar.YEAR);

            String st = "";
            int mod = diff % 10;

            if (mod == 1)
                st = " год";
            else if (mod > 1 && mod < 5)
                st = " года";
            else
                st = " лет";

            return String.format("%d %s", diff, st);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "-";
    }

    private String normalize(String text) {
        return StringUtils.capitalize(StringUtils.lowerCase(text));
    }
}
