package test.my.employees.data.remote.models;

import java.util.ArrayList;
import java.util.List;

import test.my.employees.data.locale.models.Employee;

/**
 * Created by Sergey Karleev on 04.11.2018.
 */
public class EmployeesList {
    List<Employee> response = new ArrayList<>();

    public List<Employee> getResponse() {
        return response;
    }

    public void setResponse(List<Employee> response) {
        this.response = response;
    }
}
