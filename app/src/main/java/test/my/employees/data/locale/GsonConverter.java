package test.my.employees.data.locale;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

import test.my.employees.data.locale.models.Specialty;

/**
 * Created by Sergey Karleev on 05.11.2018.
 */
public class GsonConverter {


    @TypeConverter
    public static List<Specialty> stringToSomeObjectList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<Specialty>>() {
        }.getType();

        return new Gson().fromJson(data, listType);
    }

    @TypeConverter
    public static String someObjectListToString(List<Specialty> someObjects) {
        return new Gson().toJson(someObjects);
    }
}
