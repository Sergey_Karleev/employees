package test.my.employees.data.locale.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import java.util.List;

import test.my.employees.data.locale.models.Employee;

/**
 * Created by Sergey Karleev on 05.11.2018.
 */
@Dao
public abstract class EmployeesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void save(List<Employee> employeesList);

    @Query("SELECT * FROM Employee")
    public abstract List<Employee> load();

    @Delete
    public abstract void delete(List<Employee> list);

    @Transaction
    public void clear() {
        delete(load());
    }

    ;
}
