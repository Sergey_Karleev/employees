package test.my.employees.data.remote.api;

import retrofit2.Call;
import retrofit2.http.GET;
import test.my.employees.data.remote.models.EmployeesList;

/**
 * Created by Sergey Karleev on 04.11.2018.
 */
public interface EmployeesApi {
    @GET("master/testTask.json")
    Call<EmployeesList> getEmployees();
}
