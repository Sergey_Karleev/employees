package test.my.employees.data.locale;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import test.my.employees.data.locale.dao.EmployeesDao;
import test.my.employees.data.locale.models.Employee;

/**
 * Created by Sergey Karleev on 05.11.2018.
 */
@Database(entities = Employee.class, version = 1, exportSchema = false)
public abstract class EmployeesDB extends RoomDatabase {
    public abstract EmployeesDao employeesDao();
}
