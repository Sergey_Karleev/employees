package test.my.employees.data.repositories;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import test.my.employees.common.ApiResponse;
import test.my.employees.data.locale.dao.EmployeesDao;
import test.my.employees.data.locale.models.Employee;
import test.my.employees.data.remote.api.EmployeesApi;
import test.my.employees.data.remote.models.EmployeesList;

/**
 * Created by Sergey Karleev on 04.11.2018.
 */
@Singleton
public class EmployeesRepository {
    private final MutableLiveData<ApiResponse<List<Employee>>> mutableLiveData = new MutableLiveData<>();
    private EmployeesApi employeesApi;
    private EmployeesDao employeesDao;


    public EmployeesRepository(EmployeesApi employeesApi, EmployeesDao employeesDao) {
        this.employeesApi = employeesApi;
        this.employeesDao = employeesDao;
    }

    public LiveData<ApiResponse<List<Employee>>> getEmployeesList() {
        refreshEmpoyeesList();
        return mutableLiveData;
    }

    public void refreshEmpoyeesList() {
        // This isn't an optimal implementation. We'll fix it later.
        employeesApi.getEmployees().enqueue(new Callback<EmployeesList>() {
            @Override
            public void onResponse(Call<EmployeesList> call, Response<EmployeesList> response) {
                List<Employee> employeesList = response.body().getResponse();
                Executors.newSingleThreadExecutor().execute(() -> {
//                    employeesDao.clear();
                    employeesDao.save(employeesList);
                });
                mutableLiveData.setValue(ApiResponse.success(employeesList));
            }

            @Override
            public void onFailure(Call<EmployeesList> call, Throwable t) {
                Future<List<Employee>> submit = Executors.newSingleThreadExecutor().submit(() -> employeesDao.load());
                try {
                    mutableLiveData.setValue(ApiResponse.error(t.getMessage(), submit.get()));
                } catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
